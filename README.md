## Introduction.
This are some of the scripts that I have created to control some of the modules to interact with my beaglebone.

## Documents to read.
- Beaglebone: http://beagleboard.org/Support/BoneScript/

## Video documentation
- https://github.com/derekmolloy/boneCV/
- http://derekmolloy.ie/beaglebone/beaglebone-video-capture-and-image-processing-on-embedded-linux-using-opencv/
- Command:  ./capture -F -c300000 -o > output.raw
