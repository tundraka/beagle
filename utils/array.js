var ArrayUtils = function() {
    'use strict';

    /*
     * Will initially handle int values.
     */

    // The array with the values.
    var arrayElements,

    initialize = function() {
        arrayElements = [];
    },

    add = function(element) {
        arrayElements.push(element);
    },

    length = function() {
        return arrayElements.length;
    },

    sortNumberAsc = function(a, b) {
        return a - b;
    },

    average = function() {
        if (arrayElements.length === 0) { return 0; }
        if (arrayElements.length === 1) { return arrayElements[0]; }

        // We will only sort when the amount of elements
        if (arrayElements.length > 3) {
            arrayElements.sort(sortNumberAsc);
            // remove the first and last elements (outliers)
            arrayElements.pop();
            arrayElements.shift();
        }

        var elementAmounts = 0;
        arrayElements.forEach(function(element) {
            elementAmounts += element;
        });

        return elementAmounts / arrayElements.length;
    };

    return {
        initialize: initialize,
        add: add,
        length: length,
        average: average
    };
};
module.exports = ArrayUtils;
