var b = require('bonescript');
var SERVO = 'P9_22';
var SERVO2 = 'P9_21';
var values = [0.05, 0.075, 0.1];
var label = ['0', '90', '180'];
var position = 0;
var freq = 50; // 50Hz.

function noop() {}

function scheduleNextUpdate(x) {
    console.log(x);
    position = position + 1;
    if (position > 2) { position = 0; }

    // call updateDuty after 200ms
    setTimeout(updateDuty, 500);
}

function updateDuty() {
    // compute and adjust dutyCycle based on
    // desired position in range 0..1
    var dutyCycle = values[position];
    console.log('Duty Cycle: ' + label[position] + ' = ' + values[position]);
    b.analogWrite(SERVO, dutyCycle, freq, noop);
    b.analogWrite(SERVO2, dutyCycle, freq, scheduleNextUpdate);
}

b.pinMode(SERVO, b.OUTPUT);
b.pinMode(SERVO2, b.OUTPUT);
updateDuty();
