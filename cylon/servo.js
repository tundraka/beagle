var Cylon = require('cylon');

// Initialize the robot
Cylon.robot({
    connections: {
        beaglebone: { adaptor: 'beaglebone' }
    },

    devices: {
        servo: {
            driver: 'servo',
            pin: 'P9_14',
            freq : 50,
            pulseWidth: { min: 500, max: 2400 },
            limits: { bottom: 20, top: 160 }
        }
    },

    work: function(my) {
        var angle = 30,
            increment = 40;

        every((1).seconds(), function() {
            angle += increment;
            my.servo.angle(angle);
            console.log("Current Angle: " + my.servo.currentAngle());
            if ((angle === 30) || (angle === 150)) { increment = -increment; }
        });
    }
}).start();
