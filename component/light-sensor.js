var b = require('bonescript'),
    ArrayUtils = require('../utils/array');

var LightSensor = function() {
    'use strict';

    var initialized = false,
        lastRead = 1, // there is light.
        pollInterval = null,

        INTERVAL = 1 * 60 * 1000, // 1 minute
        SHORT_INTERVAL = 0.5 * 1000, // second

        // Configuration.
        LIGHT_PORT = null,

        // TODO improvement, ready several times, remove min/max, calculate avg.
        arrayUtils = null,
        MAX_SHORT_READS = 10,
        readInterval = null,

        // TODO improvement, keep several values stored and calculate the average
        // based on those.

    terminate = function () {
        initialized = false;
        clearInterval(pollInterval);
        clearInterval(readInterval);
    },

    ready = function () {
        return initialized;
    },

    processLight = function(lightAmount) {
        if (!lightAmount) { return; }

        var lightPercentage = Math.round(lightAmount.value * 100) / 100;
        arrayUtils.push(lightPercentage);
    },

    shortRead = function() {
        if (arrayUtils.length() < MAX_SHORT_READS) {
            b.analogRead(LIGHT_PORT, processLight);
        } else {
            lastRead = arrayUtils.average();
            arrayUtils.initialize();

            clearInterval(readInterval);
            initialized = true;
        }
    },

    readLight = function () {
        readInterval = setInterval(shortRead, SHORT_INTERVAL);
    },

    lightAmount = function() {
        return lastRead;
    },

    init = function(options) {
        if (!options) { return; }

        LIGHT_PORT = options.port || null;

        if (LIGHT_PORT == null) { return; }

        arrayUtils = new ArrayUtils();
        arrayUtils.initialize();

        readLight();
        pollInterval = setInterval(readLight, INTERVAL);
    };

    return {
        init: init,
        terminate: terminate,
        ready: ready,

        lightAmount: lightAmount
    };
};

module.exports = LightSensor;
