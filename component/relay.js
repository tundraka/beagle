var b = require('bonescript');

var RelayController = function() {
    'use strict';

    var initialized = false,

        LOW = b.LOW,
        HIGH = b.HIGH,

        RELAY_PORT = null,
        // relay initial state open=true
        RELAY_OPEN = null,


    ready = function() {
        return initialized;
    },

    state = function(status) {
        if (!ready()) { return; }
        b.digitalWrite(RELAY_PORT, status);
    },

    close = function() {
        state(LOW);
    },

    open = function() {
        state(HIGH);
    },

    initializeRelay = function() {
        if (RELAY_OPEN) { open(); }
        else { close(); }
    },

    init = function(options) {
        if (!options) { return; }

        RELAY_PORT = options.port || null;
        RELAY_OPEN = options.open || null;

        if (RELAY_PORT == null || RELAY_OPEN == null) { return; }

        initialized = true;

        b.pinMode(RELAY_PORT, b.OUTPUT);
        initializeRelay();
    },

    terminate = function() {
        initializeRelay();
        initialized = false;
    };

    return {
        init: init,
        terminate: terminate,
        ready: ready,

        open: open,
        close: close
    };
};

module.exports = RelayController;
