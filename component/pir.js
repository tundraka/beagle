var b = require('bonescript');

var PirController = function() {
    'use strict';

    var
        // value that the PIR produces when there's motion.
        MOTION = 1,
        // Time that the PIR takes to consider it calibrated.
        CALIBRATE_PERIOD = 4 * 1000,

        initialized = false,
        pollInterval = null,
        motionDetected = false,
        motionDate = 0,

        // The port where the PIR will be connected.
        PIRPORT = null,
        // How often is the PIR ging to be poll for motion in millis.
        POLL_PERIOD = null,

    processPirRead = function(pirRead) {
        if (pirRead) {
            motionDetected = (pirRead.value === MOTION);
            if (motionDetected) { motionDate = Date.now(); }
        } else {
            motionDetected = false;
        }
    },

    ready = function() {
        return initialized;
    },

    readPir = function() {
        if (!ready()) { return; }

        b.digitalRead(PIRPORT, processPirRead);
    },

    init = function(options) {
        if (!options) { return; }

        PIRPORT = options.port || null;
        POLL_PERIOD = options.checkPeriod || null;

        if (PIRPORT == null || POLL_PERIOD == null) { return; }

        b.pinMode(PIRPORT, b.INPUT);
        pollInterval = setInterval(readPir, POLL_PERIOD);

        setTimeout(function() { initialized = true; }, CALIBRATE_PERIOD);
    },

    terminate = function () {
        clearInterval(pollInterval);
        motionDetected = false;
        init = false;
    },

    isMotionDetected = function() {
        return motionDetected;
    },

    millisSinceLastMotion = function() {
        return Date.now() - motionDate;
    };

    return {
        init: init,
        terminate: terminate,
        ready: ready,

        isMotionDetected: isMotionDetected,
        millisSinceLastMotion: millisSinceLastMotion
    };
};

module.exports = PirController;
