var childProcess = require('child_process');
    //events = require('events');

var Camera = function() {
    'use strict';

    var
        FRAMES = 0,

    initialize = function(options) {
        if (!options) { return; }

        FRAMES = options.frames || null;

        if (!FRAMES) { return; }
    },

    terminate = function() {
    },

    record = function() {
        console.log('recording');
        var capture = childProcess.spawn('commands/capture',
                                         ['-F', '-c' + FRAMES, '-o>output.raw']);
        capture.on('exit', function() {
            console.log('we have a recording.');
            // TODO pass it to PHP.
        });
    };

    return {
        initialize: initialize,
        terminate: terminate,
        record: record
    };
};


module.exports = Camera;
