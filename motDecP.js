var
    // Components.
    PirController = require('./component/pir'),
    RelayController = require('./component/relay'),
    LightReader = require('./component/light-sensor'),
    // Tools
    Logger = require('./tools/logger');

var Master = function() {

    'use strict';

    var POLL_INTERVAL = 1 * 1000,// 1 second
        LOCK = 5 * 60 * 1000, // 5 min
        CHECK_INTERVAL = 5 * 60 * 1000, // 5 min

        ENOUGH_LIGHT_THRESHOLD = 0.16,
        LIGHT_ON_AMOUNT = 0.02,

        pirController = null,
        relayController = null,
        lightReader = null,
        log = null,
        pollInterval = null,

        motionDetected = function() {
            return pirController.millisSinceLastMotion() < LOCK;
        },

        enoughLight = function(lightOn) {
            var lightFactor = (lightOn) ? LIGHT_ON_AMOUNT : 0,
                lightAmount = (lightReader.lightAmount() - lightFactor);

            return (lightAmount >= ENOUGH_LIGHT_THRESHOLD);
        },

        ready = function() {
            return (pirController.ready()
                    && relayController.ready()
                    && lightReader.ready());
        },

        poll = function() {
            if (!ready()) {
                log.info('Waiting for modules to initialize.');
                return;
            }

            // If the last motion detected is less than 5 min, we report that there
            // was movement.
            if (enoughLight()) {
                log.info('Enough light, turning lights out');
                relayController.open();
            } else if (motionDetected()) {
                log.info('Motion detected and not enough light ('
                         + lightReader.lightAmount() + '), turning on.');
                relayController.close();

                // Can it be the case that while I'm setting a new interval the
                // previous one gets called?
                log.info('Entering a ' + CHECK_INTERVAL + ' sleep period.');
                // TODO do I need to clear the interval before assigning a new one?
                clearInterval(pollInterval);
                pollInterval = setInterval(poll, CHECK_INTERVAL);
            }
        },

        terminate = function () {
            log.info('Terminating master module and submodules.');

            pirController.terminate();
            relayController.terminate();
            lightReader.terminate();

            log.info('Killing master interval.');
            clearInterval(pollInterval);

            log.info('Terminated.');
        },

        init = function() {
            log = new Logger();
            pirController = new PirController();
            relayController = new RelayController();
            lightReader = new LightReader();

            log.info('Initializing modules.');
            pirController.init({port: 'P8_19', checkPeriod: 1000});
            relayController.init({port: 'P8_17', open: true});
            lightReader.init({port: 'P9_40'});

            log.info('Setting master interval to ' + POLL_INTERVAL + 'ms.');
            pollInterval = setInterval(poll, POLL_INTERVAL);
        };

        return {
            init: init,
            terminate: terminate
        };
    },

    master = new Master();
    master.init();

process.on('SIGINT', function() {

    'use strict';

    console.log('Shutting down, wait...');
    master.terminate();

    setTimeout(function() {
        console.log('Done.');
        process.exit(); // eslint-disable-line no-process-exit
    }, 4000);
});
