var PirController = require('component/pir'),
    CameraController = require('component/camera');
var
    Camera = function() {
        'use strict';
        var CAMERA_POLL_INTERVAL = 1 * 1000, // Every second
            pirController = null,
            cameraController = null,
            cameraInterval = null,

        poll = function() {
            if (pirController.isMotionDetected()) {
                cameraController.record();
            }
        },

        terminate = function() {
            pirController.terminate();
            cameraController.terminate();
            clearInterval(cameraInterval);
        },

        init = function() {
            pirController = new PirController();
            cameraController = new CameraController();

            // TODO set the port for the pir.
            pirController.initialze({port: '', checkPeriod: 1000 });
            cameraController.initialize({frames: 30000});
            cameraInterval = setInterval(poll, CAMERA_POLL_INTERVAL);
        };

        return {
            init: init,
            terminate: terminate
        };
    },
    camera = new Camera();

camera.init();


process.on('SIGINT', function() {
    'use strict';

    console.log('Shutting down, wait...');
    camera.terminate();

    setTimeout(function() {
        console.log('Done.');
        process.exit(); // eslint-disable-line  no-process-exit
    }, 4000);
});
