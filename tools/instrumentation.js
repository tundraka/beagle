var Instrumentation = function() {
    var elements = [],

    add = function(key, data) {
        elements[key] = data;
    };

    return {
        toString : toString,
        parts : parts
    }
};

module.exports = Instrumentation;
