var fs = require('fs');

var Logger = function() {
    'use strict';

    var
        // TODO move to conf
        LOG_FILE = './motDecP.log',

    appendError = function(err) {
        if (err) {
            console.log('Error writing log: ' + err);
        }
    },

    log = function(message) {
        if (message && message.trim().length > 0) {
            var date = new Date();
            fs.appendFile(LOG_FILE, date.toISOString() + ':' + message + '\n', appendError);
        }
    };

    return {
        debug: log,
        info: log,
        warn: log,
        error: log
    };
};

module.exports = Logger;
